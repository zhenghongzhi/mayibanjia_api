// 'use strict';
var Express = require('express');
var BodyParser = require('body-parser');
var Swaggerize = require('swaggerize-express');
var multer = require('multer');
var Path = require('path');
var cors = require('cors')
var App = Express();
var ip = require("./utils/ip");
var log = require("./utils/logger");
var uploadImg = require("./controllers/upload/uploadImg");
var sd = require("silly-datetime");
var ApiError = require("./utils/ApiError.js");
var ApiResult = require("./utils/ApiResult.js");
var light = require("./utils/util.js");
var dataBase = require("./db/initDB");
var Path = require("path");
var ApiLog = require("./db/ApiLog.js");
dataBase.Init();
console.log("创建数据库语句:" + "CREATE DATABASE weitixing0906 DEFAULT CHARACTER SET utf8mb4 -- UTF-8 Unicode;")
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads');
    },
    filename: function(req, file, cb) {
        var ext = Path.extname(file.originalname);
        var name = file.originalname.replace(ext, "");
        var currenttime = sd.format(new Date(), "YYYYMMDDHHmmss");
        var newName = name + "." + currenttime + ext;
        cb(null, newName);
    }
});
light.mkdir('./public/uploads');
var upload = multer({
    storage: storage
});

App.use(cors()) //解决跨域

App.use(BodyParser.json({ limit: '10mb' }));

App.use(Express.static('public'));

App.use(BodyParser.urlencoded({
    extended: true
}));


//上传文件
App.post('/v1/upload/uploadImg', upload.array('file'), async function(req, res, next) {
    uploadImg.post(req, res, next);
});

//https://stackoverflow.com/questions/40747951
App.use(function(req, res, next) {
    var startTime = Date.now();

    function unhandledRejection(reason, p) {
        console.error('unhandledRejection ', p, " reason: ", reason);
        preSend(req, res, startTime, reason)
        next(reason);
    }

    function uncaughtException(reason, p) {
        console.error('uncaughtException ', p, " reason: ", reason);
        preSend(req, res, startTime, reason)
        next(reason);
    }
    process.on('uncaughtException', uncaughtException);
    process.on('unhandledRejection', unhandledRejection);

    var end = res.end;
    res.end = function(chunk, encoding) {
        // Prevent MaxListener on process.events
        process.removeListener('uncaughtException', uncaughtException);
        process.removeListener('unhandledRejection', unhandledRejection);
        res.end = end;
        res.end(chunk, encoding);
    };
    next();
});

App.all("*", function(req, res, next) {
    var startTime = Date.now();
    var _send = res.send;
    res.send = function() {
        preSend(req, res, startTime, arguments[0])
        return _send.apply(res, arguments);
    };
    next();
});

function preSend(req, res, startTime, result) {
    if (req.path.indexOf("apilogs") >= 0) {
        return;
    }
    if (res.statusCode == 200) {
        if (result.code == 0) {
            return;
        }
    }
    var execTime = Date.now() - startTime;
    var body = "";
    if (typeof(result) == "object") {
        result = JSON.stringify(result)
    }
    if (typeof(req.body) == "object") {
        body = JSON.stringify(req.body)
    }
    // res.set('X-Execution-Time', String(execTime));
    var apiLog = {
        RequestUri: req.path,
        Method: req.method,
        Body: body,
        RequestTime: startTime,
        Token: req.headers["x-openid"] || req.headers["x-token"],
        ClientIP: ip.getClientIP(req),
        Result: result,
        HttpStatusCode: res.statusCode || -1,
        ExecTime: execTime,
    };
    ApiLog.create(apiLog);
}
App.use(Swaggerize({
    api: Path.resolve('./config/swagger.json'),
    docspath: '/api-docs',
    handlers: Path.resolve('./controllers'), //正式环境
    //     handlers: Path.resolve('./handlers') //Mock环境
    security: './security'
}));

App.use(function(req, res, next) {
    var startTime = Date.now();
    var _send = res.send;
    
    res.send = function() {
        preSend(req, res, startTime, "404")
        return _send.apply(res, arguments);
    };
    res.status(404).send('can not find the path');
});

//http://expressjs.com/zh-cn/guide/error-handling.html
App.use(function(err, req, res, next) {
    if (req.path != "/v1/upload/uploadImg") {
        if (err.status) {
            res.status(err.status).send({ code: -2, message: err.message });
        } else {
            next(err);
        }
        console.error(err);
    }
});

App.listen(8000, function(req, res, next) {
    ErrorToJson();
    App.swagger.api.host = 'localhost:' + this.address().port;
    /* eslint-disable no-console */
    console.log('App running on localhost:%d', this.address().port);
    console.log('接口文档地址暂为:http://petstore.swagger.io/，输入http://localhost:8000/v1/api-docs 并点击explore');
    /* eslint-disable no-console */
});

function ErrorToJson() {
    if (!('toJSON' in Error.prototype))
        Object.defineProperty(Error.prototype, 'toJSON', {
            value: function() {
                var alt = {};
                Object.getOwnPropertyNames(this).forEach(function(key) {
                    alt[key] = this[key];
                }, this);
                return alt;
            },
            configurable: true,
            writable: true
        });
}