///查询条件
///$eq 表示等于,$ne表示不等于,$gt表示大于,$gte表示大于等于,$lt表示小于,$lte表示小于等于
//$between表示在两个数之间,$notbetween表示不再两个数之间,$in表示符合的所有,$notin表示不符合的所有
//$like表示模糊符合,$notLike表示不符合模糊提交
//支持的类型，string 字符串,datetime 时间格式




function setQueryCfg(req, res, next) {
    var queryCfg = req.body;
    if (queryCfg.length > 0) {
        var testWhere = {
            '$and': []
        };
        for (var i = 0; i < queryCfg.length; i++) {
            var op = {};
            var where = {};
            var element = queryCfg[i];
            if (element.field) {
                if (op1.indexOf(element.op) >= 0) {
                    op[element.op] = element.value[0];
                    where[element.field] = op;

                }

                if (op2.indexOf(element.op) >= 0) {
                    op[element.op] = element.value;
                    where[element.field] = op;
                }

                if (op3.indexOf(element.op) >= 0) {
                    op[element.op] = '%' + element.value[0] + '%';
                    where[element.field] = op;
                }
                testWhere['$and'].push(where);
            }

        }
        req.queryCfg = testWhere;
    }
}

exports.queryCfg = setQueryCfg;