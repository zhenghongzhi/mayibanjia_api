var log4js = require('log4js');
var log4js_config = require("../config/log4js.json");
log4js.configure(log4js_config);

/**
 * var logger = require('./utils/logger').getLogger();
logger.trace('This is a Log4js-Test');
logger.debug('We Write Logs with log4js');
logger.info('You can find logs-files in the log-dir');
logger.warn('log-dir is a configuration-item in the log4js.json');
logger.error('In This Test log-dir is : \'./logs/log_test/\'');
 */

module.exports = {
    getLogger: function(loggerName) {
        loggerName = loggerName || "error";
        return log4js.getLogger(loggerName);
    }
}