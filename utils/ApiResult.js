module.exports = class ApiResult {
    constructor(code, message, data) {
        this.code = code || 0;
        this.message = message || "操作成功";
        if (typeof(data) != "undefined") {
            this.data = data;
        }
    }
}