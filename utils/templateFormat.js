var log = require("./logger");
module.exports = {
    format: function tplFormat(content, callback) {
        var array = content.replace(/\r|\n/ig, '').match(/{{.*?(?=.DATA}})/g);
        var contentList = [];
        for (var i = 0; i < array.length; i++) {
            contentList.push(array[i].split('{{')[1]);
        }
        return contentList;
    },
    AllFormat: function tplAllFormat(content, demo) {
        //匹配 [.\n].*(?=.DATA}})
        //匹配字段属性 [.\n].*(?={{)

        var contentList = [];
        content = '\n' + content;
        var arrayAll = content.match(/[.\n].*(?=.DATA}})/g);
        var demoList = demo.match(/(^.*?\n)|(\n[^\n]*$)/g);
        var array = [];
        if (arrayAll != null && arrayAll.length > 0) {
            for (var i = 0; i < arrayAll.length; i++) {
                if (arrayAll[i] != "") {
                    array.push(arrayAll[i].replace(/\r|\n/ig, ''));
                }
            }

            for (var i = 0; i < array.length; i++) {
                var data = array[i].split('{{');
                var content = {};
                if (data[1] == "first") {
                    content["key"] = 'first';
                    content["default"] = demoList[0];
                    content["label"] = '副标题';
                } else {
                    if (data[1] == "remark") {
                        content["key"] = 'remark';
                        content["default"] = demoList[0];
                        content["label"] = '消息备注';
                    } else {
                        content["key"] = data[1];
                        content["default"] = '';
                        content["label"] = data[0].replace("：", "").replace(":", "");
                    }
                }
                contentList.push(JSON.stringify(content));
            }
        } else {
            contentList = [];
        }
        return contentList;
    }
};