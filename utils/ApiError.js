module.exports = class ApiError extends Error {
    constructor(msg) {
        super(msg)
        this.message = msg
        this.name = "ApiError";
    }
}