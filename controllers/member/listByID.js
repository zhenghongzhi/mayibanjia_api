'use strict';
var members = require("../../db/member.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /member/listByID
 */
module.exports = {
    /**
     * summary: 根据会员编号显示会员列表
     * description: 
     * parameters: pageSize, pid
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function listMemberByID(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var pageParams = light.getPageParams(req.url);
        pageParams.pageSize = 100000;
        //得到where条件
        var condition = light.getCondition([{ "field": "memberNo", "op": "$like", "value": [pageParams.memberNo] },
            { "field": "status", "op": "$ne", "value": [-1] }
        ]);
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = condition;
        var memberList = await members.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", memberList));
    }
};