'use strict';
var members = require("../../db/member.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");

/**
 * Operations on /member/{id}
 */
module.exports = {
    /**
     * summary: 更新会员
     * description: 更新会员信息
     * parameters: id, body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    put: async function updateMember(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */

        try {
            var curUser = req.curUser;
            var memberID = light.getPathParam(req.path, 3)
            if (!memberID) {
                throw new ApiError("会员ID不可缺少");
            }
            var body = req.body;
            // var findMemberNo = await members.findOne({ where: { memberNo: body.memberNo, status: { '$ne': -1 }, id: { '$ne': memberID } } });
            // if (findMemberNo != null) {
            //     throw new ApiError("会员编号已存在");
            // }
            body.updatedBy = curUser.id;
            var result = await members.update(body, { where: { id: memberID } });
            if (result.length == 0) {
                throw new ApiError("更新失败。memberID：" + memberID);
            }
            return res.status(200).send(new ApiResult(0, '更新成功'));

        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }
    },
    /**
     * summary: 删除会员
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    delete: async function deleteMember(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */

        try {
            var memberID = light.getPathParam(req.url, 3);
            if (!memberID) {
                throw new ApiError("会员ID不可缺少")
            }
            await members.update({ status: -1 }, { where: { id: memberID } });
            return res.status(200).send(new ApiResult(0, '删除成功'));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }
};