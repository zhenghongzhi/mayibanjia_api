'use strict';
var members = require("../../db/member.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");

/**
 * Operations on /member/detail
 */
module.exports = {
    /**
     * summary: 会员详情
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function detailMember(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var paramObj = light.param2Obj(req.url);
        var member = await members.findOne({ where: { id: paramObj.id } });
        return res.status(200).send(new ApiResult(0, '查询成功', member));
    }
};