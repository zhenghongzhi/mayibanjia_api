'use strict';
var members = require("../../db/member.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /member/checkMemberNo
 */
module.exports = {
    /**
     * summary: 判断会员编号是否存在
     * description: 
     * parameters: memberNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function checkMemberNo(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var paramObj = light.param2Obj(req.url);
        var memberNo = paramObj.memberNo;
        var where = { memberNo: memberNo };
        where["status"] = { '$ne': -1 };
        if (paramObj.id) {
            where["id"] = { '$ne': paramObj.id };
        }
        var member = await members.findOne({ where: where });
        if (member == null) {
            return res.status(200).send(new ApiResult(0, memberNo + "不存在"));
        } else {
            return res.status(200).send(new ApiResult(-1, member.memberNo + "已存在"));
        }
    }
};