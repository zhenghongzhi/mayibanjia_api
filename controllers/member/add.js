'use strict';
var members = require("../../db/member.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var sd = require("silly-datetime");

module.exports = {
    /**
     * summary: 添加会员
     * description: 
     * parameters: body
     * produces: application/json
     * responses: 200, 400, 404
     */
    post: async function createMember(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var member = req.body;
            // if (!member.memberNo) {
            //     member.memberNo = "";
            // }
            // var findMemberNo = await members.findOne({ where: { memberNo: member.memberNo, status: { '$ne': -1 } } });
            // if (findMemberNo != null) {
            //     throw new ApiError("会员编号已存在");
            // }
            member.status = 0;
            member.createdBy = curUser.name;
            member = await members.create(member);
            var result = await members.update({ memberNo: getMemberNo(member.id) }, { where: { id: member.id } });
            return res.status(200).send(new ApiResult(0, '添加成功'));

            function getMemberNo(id) {
                switch (id.toString().length) {
                    case 1:
                        return "CH-0000" + id.toString();
                    case 2:
                        return "CH-000" + id.toString();
                    case 3:
                        return "CH-00" + id.toString();
                    case 4:
                        return "CH-0" + id.toString();
                }
                return "CH-" + id.toString();
            }
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }
};