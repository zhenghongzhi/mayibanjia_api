var members = require("../../db/member.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var query = require("../../utils/queryCfg");
var labels = require("../../db/label.js");
var fans = require("../../db/fan.js");
module.exports = {
    /**
     * summary: 会员列表
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function listMember(req, res, next) {
        var labelInclude = { model: labels };
        var whereParams = [{ "field": "status", "op": "$in", "value": ["0"] }];
        if (req.body) {
            whereParams = whereParams.concat(req.body);
            var item = whereParams.find(x => x.field == "industryID");
            if (item != null) {
                item.field = "name";
                labelInclude = {
                    model: labels,
                    where: light.getCondition([item])
                };
            }
            whereParams = whereParams.filter(x => x.field != "name");
        }
        var pageParams = light.getPageParams(req.url);
        //得到where条件
        var condition = light.getCondition(whereParams);
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = condition;
        queryEntity.include = [{ model: fans }, labelInclude]
        queryEntity.distinct = true;
        var list = await members.findAndCountAll(queryEntity);
        res.status(200).send(new ApiResult(0, "查询成功", list));
    }
};