 'use strict';
 var members = require("../../../db/member.js");
 var light = require("../../../utils/util.js");
 var ApiError = require("../../../utils/ApiError.js");
 var ApiResult = require("../../../utils/ApiResult.js");
 /**
  * Operations on /member/{id}
  */
 module.exports = {
     /**
      * summary: 更新会员
      * description: 更新会员信息
      * parameters: id, body
      * produces: application/xml, application/json
      * responses: 200, 400, 404
      */
     put: async function BindOpenId(req, res, next) {
         /**
          * Get the data for response 200
          * For response `default` status 200 is used.
          */
         try {
             var curUser = req.curUser;
             var memberID = light.getPathParam(req.path, 4)
             if (!memberID) {
                 throw new ApiError("会员ID不可缺少");
             }
             var openID = req.body.openID;
             var exsit = await members.findOne({ where: { openID: openID, status: { '$ne': -1 } } });
             if (exsit) {
                 throw new ApiError("OpenId已绑定给" + exsit.companyName + "。无法重复绑定");
             }
             var result = await members.update({
                 openID: openID,
                 updatedBy: curUser.id
             }, { where: { id: memberID } })
             if (result.length == 0) {
                 throw new ApiError("绑定失败。memberID：" + memberID);
             }
             return res.status(200).send(new ApiResult(0, '绑定成功'));

         } catch (error) {
             if (error instanceof ApiError) {
                 return res.status(200).send(new ApiResult(-1, error.message));
             }
             throw error;
         }
     },
     /**
      * summary: 删除会员
      * description: 
      * parameters: id
      * produces: application/xml, application/json
      * responses: 200, 400, 404
      */
     delete: async function UnBindOpenId(req, res, next) {
         /**
          * Get the data for response 200
          * For response `default` status 200 is used.
          */

         try {
             var curUser = req.curUser;
             var memberID = light.getPathParam(req.path, 4)
             if (!memberID) {
                 throw new ApiError("会员ID不可缺少");
             }
             var result = await members.update({
                 openID: "openid",
                 updatedBy: curUser.id
             }, { where: { id: memberID } })
             if (result.length == 0) {
                 throw new ApiError("取消绑定失败。memberID：" + memberID);
             }
             return res.status(200).send(new ApiResult(0, '取消绑定成功'));
         } catch (error) {
             if (error instanceof ApiError) {
                 return res.status(200).send(new ApiResult(-1, error.message))
             }
             throw error;
         }
     }
 };