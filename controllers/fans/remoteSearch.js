'use strict';
var fans = require("../../db/fan.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
module.exports = {
    /**
     * summary: 根据昵称查询粉丝
     * description: 
     * parameters: pageSize, nickName
     * produces: application/json
     * responses: 200, 400, 404
     */
    get: async function remoteSearch(req, res, next) {

        var pageParams = light.getPageParams(req.url);
        var andParams = [{ "field": "nickName", "op": "$like", "value": [pageParams.nickName] }];
        var orParams = [];
        //得到where条件
        var condition = light.getCondition(andParams, orParams)
            //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = condition;

        var fanList = await fans.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, '查询成功', fanList));
    }
};