'use strict';
var fans = require("../../db/fan.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /fans/list
 */
module.exports = {
    /**
     * summary: 获取粉丝
     * description: 
     * parameters: pageSize, pageNo, searchText
     * produces: application/json
     * responses: 200, 400, 404
     */
    get: async function getFans(req, res, next) {
        var pageParams = light.getPageParams(req.url);
        //得到where条件
        var andParams = [{ "field": "openid", "op": "$ne", "value": ["openid"] }];
        var orParams = [];
        if (pageParams.searchText) {
            andParams.push({ "field": "nickname", "op": "$like", "value": [pageParams.searchText] })
        }
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = light.getCondition(andParams, orParams);;

        var fanList = await fans.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", fanList));

    }
};