'use strict';
var fans = require("../../db/fan.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var wxApi = require("../weixin/common");
var logger = require('../../utils/logger').getLogger();
/**
 * Operations on /fans/sync
 */
module.exports = {
    get: async function syncFans(req, res, next) {
        try {
            var curUser = req.curUser;
            getFans("", [], function(openIds) {

                //移除取消关注的粉丝
                deleteFans(openIds);
                var index = 0; //用来判断是否执行完成

                //得到openIds 的列表 并分组（每次只能拉取100个用户信息）
                var groupOpenIds = groupOpenIds(openIds);
                groupOpenIds.forEach(ids => {
                    wxApi.batchGetUsers(ids, function(err, result) {
                        if (err) {
                            logger.error('batchGetUsers Error : ' + err);
                            throw new ApiError(err);
                        }
                        result.user_info_list.forEach(async(x) => {
                            x.openID = x.openid;
                            x.groupID = x.groupid;
                            x.headImgUrl = x.headimgurl;
                            x.subscribeTime = x.subscribe_time;
                            x.createdBy = curUser.id;
                            x.updatedBy = curUser.id;
                            var r = await fans.upsert(x, { openID: x.openid });
                            index = r ? index + 1 : index + 1;
                            if (index == openIds.length) {
                                res.status(200).send(new ApiResult(0, "同步成功"));
                            }
                        });

                    });
                });

                function groupOpenIds(openIds) {
                    var index = 0;
                    var newArray = [];
                    while (index < openIds.length) {
                        newArray.push(openIds.slice(index, index += 100));
                    }
                    return newArray;
                }
                //移除取消关注的粉丝
                async function deleteFans(openIds) {
                    // var deleteFans = await fans.destroy({ where: { openID: { op: "$notIn", openIds } } })
                }
            });
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
        //通过OpenId获取用户信息

        function getFans(nextOpenid, openIds, callback) {
            wxApi.getFollowers(nextOpenid, function(err, res) {
                if (err) {
                    throw new ApiError(err);
                }
                openIds = openIds.concat(res.data.openid);
                if (res.data.count == 10000) {
                    getFans(res.data.nextOpenid, openIds);
                } else {
                    callback(openIds);
                }
            });
        }


    }
};