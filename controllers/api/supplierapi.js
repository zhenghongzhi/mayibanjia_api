var members = require("../../db/member.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
module.exports = {
    /**
     * summary: 标签列表
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function listLabel(req, res, next) {
        var pageParams = light.getPageParams(req.url);

        var andParams = [{ "field": "status", "op": "$in", "value": [0] }];
        var orParams = [];
        //得到where条件
        if (pageParams.searchText) {
            orParams = [
                { "field": "name", "op": "$like", "value": [pageParams.searchText] }
            ];
        }
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = light.getCondition(andParams, orParams);
        var labelList = await members.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", labelList));
    }
};