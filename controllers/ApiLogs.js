'use strict';
var ApiLog = require("../db/ApiLog.js");
var light = require("../utils/util.js");
var ApiError = require("../utils/ApiError.js");
var ApiResult = require("../utils/ApiResult.js");
module.exports = {
    /**
     * summary: 模版列表
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function GetApiLogs(req, res, next) {
        var pageParams = light.getPageParams(req.url);
        //得到where条件
        var andParams = [];
        var orParams = [];
        if (pageParams.searchText) {
            orParams.push({ "field": "RequestUri", "op": "$like", "value": [pageParams.searchText] });
            orParams.push({ "field": "Body", "op": "$like", "value": [pageParams.searchText] });
            orParams.push({ "field": "HttpStatusCode", "op": "$like", "value": [pageParams.searchText] });
            orParams.push({ "field": "Result", "op": "$like", "value": [pageParams.searchText] });
            orParams.push({ "field": "Token", "op": "$like", "value": [pageParams.searchText] });
        }
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = light.getCondition(andParams, orParams);
        var apiLogs = await ApiLog.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", apiLogs));
    }
};