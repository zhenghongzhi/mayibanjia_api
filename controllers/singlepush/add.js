'use strict';
var singlemessages = require("../../db/singlemessage.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var sd = require("silly-datetime");
var crypto = require("crypto");
var conf = require("../../config/config");
var sync = require("async");
/**
 * Operations on /singlepush/add
 */
module.exports = {
    /**
     * summary: 添加消息
     * description: 
     * parameters: body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function addSinglePush(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        //传递过来的模版内容字段:
        //fields:[{
        //     FieldName:'first',FieldValue:'你好',FieldText:'副标题'
        // }]
        try {
            var curUser = req.curUser;
            var model = req.body;
            model.status = -1;
            model.createdBy = curUser.id;
            if (!model.params || Object.keys(model.params).length == 0) {
                throw new ApiError("模板参数不能为空");
            }
            model.params = JSON.stringify(model.params);
            var singlemessage = await singlemessages.create(model);
            return res.status(200).send(new ApiResult(0, "添加成功"));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }
};