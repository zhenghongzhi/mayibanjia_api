'use strict';
var singlepush = require("../../db/singlemessage.js");
var members = require("../../db/member.js");
var templates = require("../../db/template.js");
var conf = require("../../config/config");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var tplFormat = require("../../utils/templateFormat");
var wxApi = require("../weixin/common");
var logger = require('../../utils/logger').getLogger();

/**
 * Operations on /singlepush/push
 */
module.exports = {
    /**
     * summary: 消息推送
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function singlePushMessage(req, res, next) {
        try {
            var curUser = req.curUser;

            var paramObj = light.param2Obj(req.url);
            var idList = paramObj.id.split('|');
            var pushList = await singlepush.findAll({
                where: light.getCondition([
                    { "field": "id", "op": "$in", "value": [idList] },
                    { "field": "status", "op": "$eq", "value": [-1] }
                ])
            });
            if (pushList.length == 0) {
                throw new ApiError("未找到可以推送的消息");
            }
            var memberIds = pushList.map(x => x.memberId);
            var mems = await members.findAll({
                where: light.getCondition([
                    { "field": "id", "op": "$in", "value": [memberIds] },
                    { "field": "status", "op": "$eq", "value": [0] }
                ])
            });
            var tplIDs = pushList.map(x => x.tplID);
            var tpls = await templates.findAll({ where: light.getCondition([{ "field": "id", "op": "$in", "value": [tplIDs] }]) });
            var successCount = 0;
            var faildCount = 0;
            pushList.forEach(x => {
                var member = mems.find(m => m.id == x.memberId);
                if (!member || !member.openID) {
                    faildCount++;
                    return;
                }
                var template = tpls.find(m => m.id == x.tplID);
                if (!template) {
                    faildCount++;
                    return;
                }
                var params = JSON.parse(x.params);
                var data = {};
                for (var key in params) {
                    data[key] = {
                        value: params[key].value
                    };
                }
                var url = conf.wechatUrl + "/#/single/" + x.id;
                var errmsg = "";
                wxApi.sendTemplate(member.openID, template.templateNo, url, data, async function(err, result) {
                    if (err) {
                        faildCount++;
                        errmsg += `OpenId:${member.openID}:${err}`
                        logger.error('sendTemplate Error : ' + err);
                        if (faildCount + successCount == pushList.length) {
                            res.status(200).send(new ApiResult(0, `推送成功${successCount}条。` + (faildCount > 0 ? `失败${faildCount}条。${errmsg}` : "")));
                        }
                        return;
                    }
                    var r = await singlepush.update({
                        status: 0,
                        pushTime: new Date(),
                        openID: member.openID,
                        updatedBy: curUser.id
                    }, {
                        where: { id: x.id }
                    });
                    r ? successCount++ : faildCount++;
                    if (faildCount + successCount == pushList.length) {
                        res.status(200).send(new ApiResult(0, `推送成功${successCount}条。` + (faildCount > 0 ? `失败${faildCount}条。${errmsg}` : "")));
                    }
                });
            })
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;

        }
    }
};