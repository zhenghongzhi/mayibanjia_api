'use strict';
var singlemessages = require("../../db/singlemessage.js");
var templates = require("../../db/template.js");
var singlefiles = require("../../db/singlefile.js");
var member = require("../../db/member.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var query = require("../../utils/queryCfg");
var Sequelize = require('sequelize');
/**
 * Operations on /singlepush/list
 */
module.exports = {
    /**
     * summary: 单个会员推送列表
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function listSinglePush(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var pageParams = light.getPageParams(req.url);
        pageParams.orderField = 'id';
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        //得到where条件
        var andParams = [{ "field": "status", "op": "$in", "value": [-1, 0, 1] }];
        if (req.body instanceof Array) {
            andParams = andParams.concat(req.body);
        }
        var memeberWhere = {};
        queryEntity.where = light.getCondition(andParams)
        if (pageParams.searchText) {
            var or = [];
            var subQuery = `(select id FROM member where memberNo like \'%${pageParams.searchText}%\' or companyName like \'%${pageParams.searchText}%\')`;
            or.push({ memberId: { $in: Sequelize.literal(subQuery) } });
            or.push({ params: { $like: `%${pageParams.searchText}%` } });
            queryEntity.where["$or"] = or;
        }
        queryEntity.include = [
            { model: templates }, 
            { model: singlefiles }, 
            { model: member, as: 'member'}
        ];
        queryEntity.distinct = true;
        var list = await singlemessages.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "同步成功", list));
    }
};