'use strict';
var singlemessages = require("../../db/singlemessage.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var sd = require("silly-datetime");
var crypto = require("crypto");
var conf = require("../../config/config");
var singleFile = require("../../db/singlefile");
/**
 * Operations on /singlepush/{id}
 */
module.exports = {
    /**
     * summary: 更新消息
     * description: 
     * parameters: id, body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    put: async function updateSingleMessage(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var id = light.getPathParam(req.path, 3);
            if (!id) {
                throw new ApiError("消息ID不可缺少");
            }
            var body = req.body;
            var fields = body.fields;
            body.updatedBy = curUser.id;
            if (body.params) {
                body.params = JSON.stringify(body.params);
            }
            var singlemessage = await singlemessages.update(body, {
                where: {
                    id: id
                }
            });
            res.status(200).send(new ApiResult(0, "更新成功"));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    },
    /**
     * summary: 复制并新建
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function copyNewSingle(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var id = light.getPathParam(req.path, 3);
            if (!id) {
                throw new ApiError("消息ID不可缺少")
            }
            var oldmsg = await singlemessages.findOne({
                where: { id: id },
                include: [{ model: singleFile }]
            });
            if (oldmsg == null) {
                return res.status(404);
            }
            var newMsg = await singlemessages.create({
                title: oldmsg.title,
                memberId: oldmsg.memberId,
                tplID: oldmsg.tplID,
                remark: oldmsg.remark,
                params: oldmsg.params,
                status: -1,
                createdBy: curUser.id
            });
            oldmsg.singlefiles.forEach(x => {
                singleFile.create({
                    singleMsgId: newMsg.id,
                    fileType: x.fileType,
                    fileName: x.fileName,
                    filePath: x.filePath,
                    createdBy: curUser.id
                });
            })
            res.status(200).send(new ApiResult(0, "复制成功"));

        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }
};