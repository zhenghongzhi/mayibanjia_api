'use strict';
var singlemessages = require("../../db/singlemessage.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var sd = require("silly-datetime");
var crypto = require("crypto");
var conf = require("../../config/config");
var sync = require("async");
/**
 * Operations on /singlepush/add
 */
module.exports = {
    /**
     * summary: 导入消息
     * description: 
     * parameters: body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function importSinglePush(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var dataList = req.body;
            for (var i=0; i<dataList.length; i++) {
                var model = dataList[i];
                model.status = -1;
                model.createdBy = curUser.id;
                if (!model.params || Object.keys(model.params).length == 0) {
                    throw new ApiError("模板参数不能为空");
                }
                model.params = JSON.stringify(model.params);
            }
            await singlemessages.bulkCreate(dataList);
            return res.status(200).send(new ApiResult(0, "导入成功"));            
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }
    }
};