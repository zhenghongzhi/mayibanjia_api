'use strict';
var fs = require("fs");
var log = require("../../utils/logger");
var sd = require("silly-datetime");
var SingleFile = require("../../db/singlefile");
var GroupFile = require("../../db/groupfile");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /upload/uploadBase64Img
 */
//解码base64图片
function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};
    if (matches == null || matches.length !== 3) {
        return new Error('图片格式不正确,无法解析');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');
    return response;
}
module.exports = {
    /**
     * summary: 上传Base64图片
     * description: 
     * parameters: upfile, id, type
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function uploadBase64Img(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var paramObj = req.body;
            if (!paramObj.upfile) {
                throw new ApiError("图片格式不正确")
            }
            var imgBuffer = decodeBase64Image(paramObj.upfile);
            if (imgBuffer.message) {
                throw new ApiError(imgBuffer.message)
            }
            var filename = "copyImage" + sd.format(new Date(), "YYYYMMDDHHmmssfff");
            if (imgBuffer.type == "image/jpeg") {
                filename += ".jpg";
            }
            if (imgBuffer.type == "image/png") {
                filename += ".png";
            }
            var filepath = 'uploads/' + filename;
            fs.writeFile("public/" + filepath, imgBuffer.data, async function(err) {
                if (err != null) {
                    throw new ApiError("base64图片保存失败:" + err)
                }
                var file;
                if (paramObj.type == 1) { //会员推送上传
                    file = await SingleFile.create({
                        singleMsgId: paramObj.id,
                        fileName: filename,
                        filePath: filepath,
                        fileType: 1
                    });
                } else { //分组推送上传
                    file = await GroupFile.create({
                        groupMsgId: paramObj.id,
                        fileName: filename,
                        filePath: filepath,
                        fileType: 1
                    });
                }
                return res.status(200).send(new ApiResult(0, "上传完成", file));
            });
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }
};