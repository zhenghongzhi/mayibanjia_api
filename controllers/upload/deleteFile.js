'use strict';
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var fs = require("fs");
var path = require("path");
var SingleFile = require("../../db/singlefile");
var GroupFile = require("../../db/groupfile");
/**
 * Operations on /delImg
 */
module.exports = {
    /**
     * summary: 删除图片
     * description: 
     * parameters: id
     * produces: 
     * responses: 200, 400, 404
     */
    delete: async function(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var paramObj = light.param2Obj(req.url);
        var file;
        if (paramObj.type == 1) {
            var file = await SingleFile.findOne({ where: { id: paramObj.id } });
            if (file == null) {
                return res.status(404);
            }
            await SingleFile.destroy({ where: { id: paramObj.id } });
        } else {
            var file = await GroupFile.findOne({ where: { id: paramObj.id } });
            if (file == null) {
                return res.status(404);
            }
            await GroupFile.destroy({ where: { id: paramObj.id } });
        }
        if (file != null) {
            //删除文件
            var filePath = path.join(__dirname, '../../') + "public/upload/" + file.filename;
            if (fs.existsSync(filePath)) {
                fs.unlinkSync(filePath);
            }
        }
        return res.status(200).send(new ApiResult(0, "删除成功"));
    }
};