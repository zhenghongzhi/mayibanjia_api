'use strict';
var SingleFile = require("../../db/singlefile");
var GroupFile = require("../../db/groupfile");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /getUploadList
 */
module.exports = {
    /**
     * summary: 查看图片
     * description: 
     * parameters: id, type
     * produces: 
     * responses: 200, 400, 404
     */
    get: async function(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var paramObj = light.param2Obj(req.url);
        var fileList = [];
        if (paramObj.type == 1) {
            fileList = await SingleFile.findAll({ where: { singleMsgId: paramObj.id } });
        } else {
            fileList = await GroupFile.findAll({ where: { groupMsgId: paramObj.id } });
        }
        res.status(200).send(new ApiResult(0, "查询成功", fileList));
    }
};