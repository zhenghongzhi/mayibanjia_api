'use strict';
var SingleFile = require("../../db/singlefile");
var GroupFile = require("../../db/groupfile");
var singleMsg = require("../../db/singlemessage");
var groupMsg = require("../../db/groupmessage");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var path = require("path");

/**
 * Operations on /uploadImg
 */
module.exports = {
    /**
     * summary: 上传图片
     * description: 
     * parameters: upfile
     * produces: 
     * responses: 200, 400, 404
     */
    post: async function(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        if (req.files.length == 0) {
            throw new ApiError("未找到文件")
        }
        var file = req.files[0];
        var fileSavename = file.filename;
        var temp = fileSavename.split(".");
        temp.splice(temp.length - 2, 1);
        var filename = temp.join(".");
        var filepath = 'uploads/' + file.filename;

        var id = req.body.id;
        var fileType = req.body.fileType;
        var type = req.body.type; //上传类型 1会员推送上传 2分组推送上传
        if (type == 1) { //会员推送上传
            var fileModel = await SingleFile.create({
                singleMsgId: id,
                fileName: filename,
                filePath: filepath,
                fileType: fileType
            });
            return res.status(200).send(new ApiResult(0, "上传完成", fileModel));
        } else { //分组推送上传
            var fileModel = await GroupFile.create({
                groupMsgId: id,
                fileName: filename,
                filePath: filepath,
                fileType: fileType
            });
            return res.status(200).send(new ApiResult(0, "上传完成", fileModel));
        }
    }
};