var accessTokens = require("../../db/accessToken.js");
var conf = require("../../config/config");
var WechatApi = require('wechat-api');

var api = new WechatApi(conf.appID, conf.appSecret, async function(callback) {
    var result = await accessTokens.findOne();
    callback(null, result);
}, async function(token, callback) {
    await accessTokens.destroy({ where: {} });
    await accessTokens.create({
        accessToken: token.accessToken,
        expireTime: token.expireTime
    });
    callback();
});

module.exports = api;