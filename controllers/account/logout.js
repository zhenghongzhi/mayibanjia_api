var users = require("../../db/user.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var weixin = require("../weixin/common");
/**
 * Operations on /account/logout
 */
module.exports = {
    /**
     * summary: 退出登录
     * description: 
     * parameters: 
     * produces: application/xml, application/json
     * responses: default
     */
    post: async function logoutUser(req, res, next) {
        res.status(200).send(new ApiResult(0, "退出登录成功"));
    }
};