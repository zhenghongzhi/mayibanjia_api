'use strict';
var users = require("../../db/user.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var crypto = require("crypto");
/**
 * Operations on /account/resetPwd
 */
module.exports = {
    /**
     * summary: 重置密码
     * description: 
     * parameters: userName
     * produces: text/json; charset=utf-8
     * responses: default
     */
    post: async function logoutUser(req, res, next) {
        /**
         * Get the data for response default
         * For response `default` status 200 is used.
         */
        var curUser = req.curUser;
        var paramObj = req.body;
        var crypPassword = crypto.createHash('md5').update("123456").digest('base64');
        var updateUser = await users.update({
            password: crypPassword,
            updatedBy: curUser.id
        }, {
            where: { id: paramObj.id }
        });
        return res.status(200).send(new ApiResult(0, "密码重置成功"));
    }
};