var users = require("../../db/user.js");
var accessToken = require("../../db/accessToken.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var ip = require("../../utils/ip.js");
var log = require("../../utils/logger.js");
var crypto = require("crypto");
var uuid = require('node-uuid');
var weixin = require('../weixin/common.js');
var sd = require("silly-datetime");
var conf = require("../../config/config");
module.exports = {
    /**
     * summary: 登录
     * description: 
     * parameters: userName, password
     * produces: text/json; charset=utf-8
     * responses: 200
     */
    post: async function loginUser(req, res, next) {
        try {
            var paramObj = req.body;
            var password = crypto.createHash('md5').update(paramObj.password.toString()).digest('base64');
            // var currenttime = sd.format(new Date(), "YYYY-MM-DD HH:mm");
            var date = new Date();

            var user = await users.findOne({
                where: {
                    userName: paramObj.userName,
                    password: password,
                }
            });
            if (user == null) {
                throw new ApiError("用户名或者密码错误")
            }
            if (user.status == -1) {
                throw new ApiError("用户已被禁用")
            }
            var date = new Date();
            if (user.token == null || user.tokenExpireTime < new Date()) {
                var _token = uuid.v1();
                user.token = _token;
                date.setDate(date.getDate() + 7); //暂默认为7天有效
                // var tokenExpireTime = sd.format(new Date(date.setMinutes(date.getMinutes() + conf.tokenExpires)), "YYYY-MM-DD HH:mm");
                await users.update({ token: _token, tokenExpireTime: date, updatedBy: user.userName }, {
                    where: { id: user.id }
                });
            }
            //log.getLogger("access").info("登录IP:" + ip.getClientIP(req));
            return res.status(200).send(new ApiResult(0, "登录成功", user.token));

        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }
};