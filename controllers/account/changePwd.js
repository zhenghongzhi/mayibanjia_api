'use strict';
var users = require("../../db/user.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var crypto = require("crypto");
/**
 * Operations on /account/changePwd
 */
module.exports = {
    /**
     * summary: 更改密码
     * description: 
     * parameters: id, newPassword, oldPassword
     * produces: text/json; charset=utf-8
     * responses: 200
     */
    post: async function changePwd(req, res, next) {
        try {
            var curUser = req.curUser;
            var paramObj = req.body;
            var oldPassword = crypto.createHash('md5').update(paramObj.oldPassword.toString()).digest('base64');
            if (curUser.password != oldPassword) {
                throw new ApiError("旧密码不正确")
            }
            var newPassword = crypto.createHash('md5').update(paramObj.newPassword.toString()).digest('base64');
            await users.update({
                password: newPassword,
                updatedBy: curUser.id
            }, {
                where: { id: curUser.id }
            });
            return res.status(200).send(new ApiResult(0, "密码修改成功"))
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }
};