'use strict';
var users = require("../../db/user.js");
var userroles = require('../../db/userrole.js');
var roles = require("../../db/role.js");
var ApiResult = require("../../utils/ApiResult.js");

/**
 * Operations on /account/findByToken
 */
module.exports = {
    /**
     * summary: 根据token获取账号信息
     * description: 
     * parameters: token
     * produces: application/json
     * responses: 200, 400, 404
     * operationId: findByToken
     */
    
    get: async function(req, res, callback) {
        var curUser = req.curUser;
        var user = await users.findOne({
            where: {
                status: 0,
                id: curUser.id
            },
            include: [{
                model: userroles,
                include: [{
                    model: roles,
                    where: {
                        status: 0,
                    }
                }]
            }]
        });
        if (user == null) {
            return res.status(404);
        }
        if (user.userroles != null) {
            user.userroles && user.userroles.forEach(z => {
                if (z.role.menus) {
                    z.role.menus = JSON.parse(z.role.menus);
                } else {
                    z.role.menus = [];
                }
            })
        }
        return res.status(200).send(new ApiResult(0, "查询成功", user));
    }
}