'use strict';
var labels = require("../../db/label.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");

/**
 * Operations on /label/add
 */
module.exports = {
    /**
     * summary: 添加标签
     * description: 
     * parameters: body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function createLabel(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var curUser = req.curUser;
        var model = req.body;
        model.status = 0;
        model.createBy = curUser.id;
        await labels.create(model);
        res.status(200).send(new ApiResult(0, '创建成功'));
    }
};