'use strict';
var labels = require("../../db/label.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var conf = require("../../config/config");
var https = require('https');
var sd = require("silly-datetime");
var Array = require('node-array');
var weixin = require("../weixin/common");
var sync = require("async");

/**
 * Operations on /label/sync
 */
module.exports = {
    /**
     * summary: 同步标签/分组
     * description: 
     * parameters: 
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: function syncLabel(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */

        sync.waterfall([
                weixin.checkToken,
                function(token, callback) {
                    if (token.access_token) {
                        var url = 'https://api.weixin.qq.com/cgi-bin/tags/get?access_token=' + token.access_token;
                        var request = https.get(url, function(resultLabel) {
                            var getData = '';
                            var currentTime = sd.format(new Date(), "YYYY-MM-DD HH:mm");
                            resultLabel.setEncoding('utf8');
                            resultLabel.on('data', function(chunk) {
                                getData += chunk;
                            });
                            resultLabel.on('end', function() {
                                var returnData = JSON.parse(getData);
                                // console.log(returnData);
                                if (returnData["tags"]) {
                                    var labelList = [];
                                    var labelIDList = [];
                                    returnData["tags"].forEach(function(element) {
                                        labelList.push({
                                            "labelID": element.id,
                                            "name": element.name,
                                            "status": 0,
                                            "createdAt": currentTime
                                        });
                                        labelIDList.push(element.id.toString());
                                    });
                                    callback(null, labelList, labelIDList);
                                } else {
                                    callback("从微信获取模版列表失败", null);
                                }
                            });
                        });
                    } else {
                        callback("access_token失效", null);
                    }
                },
                function(labelList, labelIDList, callback) {
                    if (labelIDList != null) {
                        labels.findAll({
                            where: {
                                labelID: {
                                    '$in': labelIDList
                                }
                            }
                        }).then(function(labelRes) {
                            labelRes.forEach(function(element) {
                                var index = labelIDList.indexOf(element.labelID);
                                if (index >= 0) {
                                    labelIDList.splice(index, 1);
                                }
                            });
                            var newlabelList = [];
                            labelList.forEach(function(element) {
                                var index = labelIDList.indexOf(element.labelID);
                                if (index >= 0) {
                                    newlabelList.push(element);
                                }
                            });
                            callback(null, newlabelList);
                        });
                    }
                },
                function(labelList, callback) {
                    if (labelList.length > 0) {
                        var label = labels.bulkCreate(labelList).then(label => {
                            if (label == null) {
                                callback("同步模版失败", null);
                            } else {
                                callback(null, label);
                            }
                        });
                    } else {
                        callback(null, labelList);
                    }
                }
            ],
            function(err, results) {
                if (err) {
                    res.status(200).send({ code: -1, message: err });
                } else {
                    var label = labels.findAndCountAll({
                        where: {},
                        offset: 0,
                        limit: 10
                    }).then(label => {
                        res.status(200).send({ code: 0, message: "同步成功", data: label });
                    });
                }
            }
        );
    }
};