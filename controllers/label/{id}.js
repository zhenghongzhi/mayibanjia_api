'use strict';
var labels = require("../../db/label.js");
var members = require("../../db/member.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /label/{id}
 */
module.exports = {
    /**
     * summary: 更新分组
     * description: 更新分组信息
     * parameters: id, body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    put: async function updateLabel(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var labelID = light.getPathParam(req.path, 3)
            if (!labelID) {
                throw new ApiError("标签ID不可缺少");
            }
            var body = req.body;
            body.updateBy = curUser.id;
            var label = await labels.update(body, {
                where: {
                    id: labelID
                }
            });
            return res.status(200).send(new ApiResult(0, '更新成功'));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }

    },
    /**
     * summary: 删除标签
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */

    delete: async function deleteLabel(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var labelID = light.getPathParam(req.url, 3);
            if (!labelID) {
                throw new ApiError("分组ID不可缺少")
            }
            var member = await members.findAll({
                where: {
                    industryID: labelID,
                    status: 0
                }
            });
            if (member != null && member.length > 0) {
                throw new ApiError("此分组下存在会员,不可删除!")
            }
            await labels.update({ status: -1 }, { where: { id: labelID } });
            return res.status(200).send(new ApiResult(0, '删除成功'));

        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }
};