var roles = require("../../db/role.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /role/add
 */
module.exports = {
    /**
     * summary: 添加权限
     * description: 只有系统管理员才拥有此权限
     * parameters: body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function createRole(req, res, next) {
        var paramObj = req.body;
        var curUser = req.curUser;
        var role = await roles.create({
            name: paramObj.name,
            roleInfo: paramObj.desc,
            status: 0,
            createdBy: curUser.id
        });
        return res.status(200).send(new ApiResult(0, '添加成功'));
    }
};