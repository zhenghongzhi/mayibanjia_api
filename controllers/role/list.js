var roles = require("../../db/role.js");
var userroles = require("../../db/userrole.js");
var rolemenu = require("../../db/rolemenu.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /role/list
 */
module.exports = {
    /**
     * summary: 权限列表
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400
     */
    get: async function listrole(req, res, next) {

        var pageParams = light.getPageParams(req.url);
        //得到where条件
        var condition = light.getCondition([{ "field": "status", "op": "$in", "value": [0] }]);
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = condition;
        var roleList = await roles.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", roleList));

    }
};