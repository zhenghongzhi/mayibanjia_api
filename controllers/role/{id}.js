'use strict';
var roles = require("../../db/role.js");
var userroles = require("../../db/userrole.js");

var rolemenu = require("../../db/rolemenu.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var light = require("../../utils/util.js");

var conf = require("../../config/config");
/**
 * Operations on /role/{id}
 */
module.exports = {
    /**
     * summary: 更新权限
     * description: 更新权限信息
     * parameters: id, body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    put: async function updateRole(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var roleID = light.getPathParam(req.url, 3);
            if (!roleID) {
                throw new ApiError("权限ID不可缺少");
            }
            var body = req.body;
            var roleData = {
                name: body.name,
                roleInfo: body.desc,
                updatedBy: curUser.id
            }
            await roles.update(roleData, { where: { id: roleID } });
            return res.status(200).send(new ApiResult(0, '更新成功'));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }
    },
    /**
     * summary: 分配菜单
     * description: 分配菜单
     * parameters: id, body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function setMenu(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var roleID = light.getPathParam(req.url, 3);
            if (!roleID) {
                throw new ApiError("权限ID不可缺少");
            }
            var roleMenus = req.body;
            await roles.update({
                menus: JSON.stringify(roleMenus),
                updatedBy: curUser.id
            }, {
                where: { id: roleID }
            });
            return res.status(200).send(new ApiResult(0, '分配菜单成功'));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }

    },
    /**
     * summary: 删除权限
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    delete: async function deleteRole(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var roleID = light.getPathParam(req.url, 3);
            if (!roleID) {
                throw new ApiError("权限ID不可缺少");
            }
            var userRoleList = await userroles.findAll({
                where: {
                    roleID: roleID
                }
            });
            if (userRoleList.length > 0) {
                throw new ApiError("此角色下存在用户,不可删除!")
            }
            await roles.update({
                status: -1,
                updatedBy: curUser.id
            }, { where: { id: roleID } });
            return res.status(200).send(new ApiResult(0, '禁用成功'));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }
    }
};