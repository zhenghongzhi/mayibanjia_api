'use strict';
// var dataProvider = require('../../data/role/detail.js');
var roles = require("../../db/role.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");

/**
 * Operations on /role/detail
 */
module.exports = {
    /**
     * summary: 查询角色详情（含角色的权限列表）
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function getRoleByID(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var paramObj = light.param2Obj(req.url);
        var role = await roles.findOne({
            where: {
                status: 0,
                id: paramObj.id
            }
        });
        if (role.menus) {
            role.menus = JSON.parse(role.menus);
        }
        return res.status(200).send(new ApiResult(0, '查询成功', role));
    }
};