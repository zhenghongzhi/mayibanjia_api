'use strict';
var light = require("../../../utils/util.js");
var ApiError = require("../../../utils/ApiError.js");
var ApiResult = require("../../../utils/ApiResult.js");
var GroupMessage = require("../../../db/groupmessage");
var templates = require("../../../db/template");
var GroupFile = require("../../../db/groupfile");
var fans = require("../../../db/fan.js");

/**
 * Operations on /message/group/{id}
 */
module.exports = {
    /**
     * summary: 消息详情
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function GetGroupDetial(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var openid = req.headers["x-openid"];
            if (!openid) {
                return res.status(403).send("Please send openid in header");
            }
            var curFan = await fans.findOne({ where: { openID: openid } });
            if (curFan == null) {
                return res.status(401).send("身份认证失败");
            }
            var id = light.getPathParam(req.url, 4);
            if (!id) {
                throw new ApiError("消息ID不可缺少");
            }
            var msg = await GroupMessage.findOne({
                where: { id: id },
                include: [{ model: GroupFile, required: false }, { model: templates, required: false }]
            });
            if (msg == null) {
                throw new ApiError("未找到消息");
            }
            return res.status(200).send(new ApiResult(0, "查询成功", msg));

        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }

    }
};