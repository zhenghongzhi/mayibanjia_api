'use strict';
var light = require("../../../utils/util.js");
var ApiError = require("../../../utils/ApiError.js");
var ApiResult = require("../../../utils/ApiResult.js");
var GroupFile = require("../../../db/groupfile");

/**
 * Operations on /message/group/{id}
 */
module.exports = {
    /**
     * summary: 消息详情
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function GetGroupFileDetial(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var id = light.getPathParam(req.url, 4);
            if (!id) {
                throw new ApiError("文件ID不可缺少");
            }
            var file = await GroupFile.findOne({
                where: { id: id },
            });
            if (file == null) {
                throw new ApiError("未找到文件");
            }
            return res.status(200).send(new ApiResult(0, "查询成功", file));

        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }

    }
};