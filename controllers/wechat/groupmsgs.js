'use strict';
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var query = require("../../utils/queryCfg");
var templates = require("../../db/template.js");
var members = require("../../db/member.js");
var fans = require("../../db/fan.js");
var labels = require("../../db/label.js");
var groupmessages = require("../../db/groupmessage.js");
var groupfiles = require("../../db/groupfile.js");
/**
 * Operations on /message/group/list
 */
module.exports = {
    /**
     * summary: 根据openID获取该用户的group推送历史
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function listGroup(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var openid = req.headers["x-openid"];
        if (!openid) {
            return res.status(403).send("Please send openid in header");
        }
        var fan = await fans.findOne({ where: { openID: openid } });
        if (fan == null) {
            return res.status(401).send("身份认证失败");
        }
        var curFan = fan;
        var pageParams = light.getPageParams(req.url);
        var member = await members.findOne({
            where: {
                status: 0,
                openID: curFan.openID
            }
        });
        if (member == null) {
            return res.status(200).send(new ApiResult(0, '未找到用户', { total: 0, rows: [] }));
        }
        //得到where条件
        var params = [
            { "field": "status", "op": "$eq", "value": [0] },
            { "field": "labelID", "op": "$eq", "value": [member.industryID] }
        ];
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = light.getCondition(params);
        queryEntity.include = [{ model: templates },
            { model: labels },
            { model: groupfiles }
        ];
        queryEntity.distinct = true;
        var list = await groupmessages.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", list));
    }
};