'use strict';
var fans = require("../../db/fan.js");
var conf = require("../../config/config.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var OAuth = require('wechat-oauth');
var client = new OAuth(conf.appID, conf.appSecret);
var wxApi = require("../weixin/common");
/**
 * Operations on /wechat/getAccessToken
 */
module.exports = {
    /**
     * summary: 获取网页授权access_token
     * description: 
     * parameters: code
     * produces: 
     * responses: 200, 404
     */
    get: async function(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var code = light.param2Obj(req.url).code;
        client.getAccessToken(code, async function(err, result) {
            if (err) {
                return res.status(200).send(new ApiResult(-1, err));
            }
            var openid = result.data.openid;
            var fan = await fans.findOne({ where: { openID: openid } });
            if (fan == null) {
                wxApi.getUser(openid, async function(err, user) {
                    if (user.subscribe == 0) {
                        return res.status(200).send(new ApiResult(-1, "请先关注公众号！"));
                    } else {
                        user.openID = user.openid;
                        user.groupID = user.groupid;
                        user.headImgUrl = user.headimgurl;
                        user.subscribeTime = user.subscribe_time;
                        // user.createdBy = curUser.id;
                        // user.updatedBy = curUser.id;
                        var r = await fans.upsert(user, { openID: user.openid });
                        return res.status(200).send(new ApiResult(0, "获取成功", result.data));
                    }
                });
            } else {
                res.status(200).send(new ApiResult(0, "获取成功", result.data));
            }
        });
    }
};