'use strict';
var fans = require("../../db/fan.js");
var conf = require("../../config/config.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var wxApi = require("../weixin/common");
/**
 * Operations on /wechat/getJsTicket
 */
module.exports = {
    /**
     * summary: getJsTicket
     * description: 
     * parameters: url
     * produces: application/json
     * responses: 200
     * operationId: getJsTicket
     */
    get: async function getJsTicket(req, res, callback) {

        var url = req.url.split('?')[1].replace("url=", "");
        url = decodeURIComponent(url);
        var param = {
            debug: false,
            jsApiList: ['previewImage'],
            url: url
        };
        wxApi.getJsConfig(param, async function(err, r) {
            if (err) {
                return res.status(200).send(new ApiResult(-1, JSON.stringify(err)));
            }
            res.status(200).send(new ApiResult(0, "获取成功", r));
        });
    }
};