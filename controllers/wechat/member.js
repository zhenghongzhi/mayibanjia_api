'use strict';
var members = require("../../db/member.js");
var fans = require("../../db/fan.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /wechat/member
 */
module.exports = {
    /**
     * summary: 微信端修改会员信息
     * description: 
     * parameters: body
     * produces: application/json
     * responses: 200, 400, 404
     */
    put: async function UpdateWXMember(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var openid = req.headers["x-openid"];
            if (!openid) {
                return res.status(403).send("Please send openid in header");
            }
            var fan = await fans.findOne({ where: { openID: openid } });
            if (fan == null) {
                return res.status(401).send("身份认证失败");
            }
            var curFan = fan;
            var member = req.body;
            var exsit = await members.findOne({ where: { openID: fan.openID, status: { '$ne': -1 } } });
            if (!exsit) {
                throw new ApiError("会员不存在");
            }
            // var findMemberNo = await members.findOne({ where: { memberNo: member.memberNo, status: { '$ne': -1 }, id: { '$ne': member.id } } });
            // if (findMemberNo != null) {
            //     throw new ApiError("会员编号已存在");
            // }
            member.status = 0;
            member.updatedBy = curFan.nickname;
            var result = await members.update(member, { where: { id: member.id } });
            if (result.length == 0) {
                throw new ApiError("更新失败。memberID：" + member.id);
            }
            return res.status(200).send(new ApiResult(0, '更新成功'));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    },
    /**
     * summary: 根据会员openID添加会员
     * description: 
     * parameters: body
     * produces: application/json
     * responses: 200, 400, 404
     */
    post: async function createWXMember(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var openid = req.headers["x-openid"];
        if (!openid) {
            return res.status(403).send("Please send openid in header");
        }
        var fan = await fans.findOne({ where: { openID: openid } });
        if (fan == null) {
            return res.status(401).send("身份认证失败");
        }
        try {
            var curFan = fan;
            var member = req.body;
            // if (!member.memberNo) {
            //     member.memberNo = new Date().getTime(), toString();
            // }
            var exsit = await members.findOne({ where: { openID: fan.openId, status: { '$ne': -1 } } });
            if (exsit) {
                throw new ApiError("无法重复添加");
            }
            // var findMemberNo = await members.findOne({ where: { memberNo: member.memberNo, status: { '$ne': -1 } } });
            // if (findMemberNo != null) {
            //     throw new ApiError("会员编号已存在");
            // }
            member.status = 0;
            member.openID = curFan.openID;
            member.createdBy = curFan.nickname;
            member = await members.create(member);
            var memberNo = getMemberNo(member.id);
            var result = await members.update({ memberNo: memberNo }, { where: { id: member.id } });
            return res.status(200).send(new ApiResult(0, '添加成功', member));

            function getMemberNo(id) {
                switch (id.toString().length) {
                    case 1:
                        return "CH-0000" + id.toString();
                    case 2:
                        return "CH-000" + id.toString();
                    case 3:
                        return "CH-00" + id.toString();
                    case 4:
                        return "CH-0" + id.toString();
                }
                return "CH-" + id.toString();
            }
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    },
    /**
     * summary: 根据会员openID获取会员详情
     * description: 
     * parameters: 
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function detailWXMember(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var openid = req.headers["x-openid"];
        if (!openid) {
            return res.status(403).send("Please send openid in header");
        }
        var fan = await fans.findOne({ where: { openID: openid } });
        if (fan == null) {
            return res.status(401).send("身份认证失败");
        }
        var curFan = fan;
        var member = await members.findOne({
            where: {
                status: 0,
                openID: curFan.openID
            }
        });
        if (member == null) {
            return res.status(200).send(new ApiResult(1, '未找到用户'));
        }
        return res.status(200).send(new ApiResult(0, '查询成功', member));
    }
};