'use strict';
var singlemessages = require("../../db/singlemessage.js");
var templates = require("../../db/template.js");
var members = require("../../db/member.js");
var fans = require("../../db/fan.js");
var singlefiles = require("../../db/singlefile.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /message/single/list
 */
module.exports = {
    /**
     * summary: 根据openID获取该用户的single推送历史
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function listSingle(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var openid = req.headers["x-openid"];
        if (!openid) {
            return res.status(403).send("Please send openid in header");
        }
        var fan = await fans.findOne({ where: { openID: openid } });
        if (fan == null) {
            return res.status(401).send("身份认证失败");
        }
        var curFan = fan;
        var pageParams = light.getPageParams(req.url);
        // var member = await members.findOne({
        //     where: {
        //         openID: curFan.openID,
        //         status: 0
        //     }
        // });
        // if (member == null) {
        //     return res.status(200).send(new ApiResult(0, '未找到用户', { total: 0, rows: [] }));
        // }
        var condition = light.getCondition([
            { "field": "openid", "op": "$eq", "value": [curFan.openID] },
            { "field": "status", "op": "$eq", "value": [0] }
        ]);
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = condition;
        queryEntity.include = [{
                model: singlefiles,
                required: false
            },
            {
                model: templates,
                required: false
            }
        ]
        queryEntity.distinct = true;
        var list = await singlemessages.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", list));
    }
};