'use strict';
var labels = require("../../db/label.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
module.exports = {
    /**
     * summary: 标签列表
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function GetLabels(req, res, next) {
        var obj = light.param2Obj(req.path);
        obj.pageSize = Number(obj.pageSize || 1000);
        obj.pageNo = Number(obj.pageNo || 1);
        obj.orderField = obj.orderField || "createdAt";
        obj.orderBy = obj.orderBy || "DESC";

        var andParams = [{ "field": "status", "op": "$in", "value": [0] }];
        var orParams = [];
        //构建查询参数
        var queryEntity = light.getQueryEntity(obj);
        queryEntity.where = light.getCondition(andParams, orParams);
        var labelList = await labels.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", labelList));
    }
};