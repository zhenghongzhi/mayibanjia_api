'use strict';
var light = require("../../../utils/util.js");
var userroles = require('../../../db/userrole.js');
var ApiError = require("../../../utils/ApiError.js");
var ApiResult = require("../../../utils/ApiResult.js");

/**
 * Operations on /user/setrole/{id}
 */
module.exports = {
    /**
     * summary: 设置权限
     * description: 
     * parameters: id, roleID
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function setrole(req, res, next) {

        try {
            var curUser = req.curUser;
            var userID = light.getPathParam(req.url, 4);
            if (!userID) {
                throw new ApiError("用户ID不可缺少");
            }
            var roleID = req.body.roleID;
            var userrole = await userroles.findOne({ where: { userID: userID } });
            if (userrole == null) {
                await userroles.create({
                    userID: userID,
                    roleID: roleID
                });
            } else {
                await userroles.update({ roleID: roleID }, { where: { userID: userID } });
            }
            return res.status(200).send(new ApiResult(0, "设置角色成功"));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }
    }
};