var users = require("../../db/user.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var userroles = require('../../db/userrole.js');
var roles = require("../../db/role.js");

/**
 * Operations on /user/detail
 */
module.exports = {
    /**
     * summary: 通过用户编号获取用户信息
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 404
     */
    get: async function getUserByID(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var paramObj = light.param2Obj(req.url);
            var user = await users.findOne({
                where: { id: paramObj.id },
                include: [{
                    model: userroles,
                    include: [{
                        model: roles,
                        where: {
                            status: 0,
                        }
                    }]
                }]
            });
            user.userroles.forEach(z => {
                z.role.menus = JSON.parse(z.role.menus);
            })
            return res.status(200).send(new ApiResult(0, '查询成功', user));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }
    }
};