var users = require("../../db/user.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var sd = require("silly-datetime");
var crypto = require("crypto");
var conf = require("../../config/config");

/**
 * Operations on /user/{id}
 */
module.exports = {
    /**
     * summary: 更新用户
     * description: 更新用户信息
     * parameters: userName, body
     * produces: application/xml, application/json
     * responses: 400, 404
     */
    post: async function updateUser(req, res, next) {
        /**
         * Get the data for response 400
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var userID = light.getPathParam(req.url, 3);
            if (!userID) {
                throw new ApiError("用户ID不可缺少");
            }
            var body = req.body;
            if (body.password != null && body.password != "") {
                body.password = crypto.createHash('md5').update(body.password.toString()).digest('base64');
            }
            body.updatedBy = curUser.id;
            await users.update(body, {
                where: { id: userID }
            });

            return res.status(200).send(new ApiResult(0, '更新成功'));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }
    },
    /**
     * summary: 删除用户
     * description: 只有系统管理员才有此权限
     * parameters: userName
     * produces: application/xml, application/json
     * responses: 400, 404
     */
    delete: async function deleteUser(req, res, next) {
        /**
         * Get the data for response 400
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var userID = light.getPathParam(req.url, 3);
            if (!userID) {
                throw new ApiError("用户ID不可缺少");
            }
            await users.update({ status: -1 }, {
                where: { id: userID }
            });
            return res.status(200).send(new ApiResult(0, '删除成功'));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }
    }
};