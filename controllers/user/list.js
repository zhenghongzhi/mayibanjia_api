var users = require("../../db/user.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var userroles = require('../../db/userrole.js');
var roles = require("../../db/role.js");

module.exports = {
    /**
     * summary: 用户列表
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400
     */
    get: async function listuser(req, res, next) {
        var pageParams = light.getPageParams(req.url);
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.include = [{
            model: userroles,
            include: [{
                model: roles,
                where: {
                    status: 0,
                }
            }]
        }];
        queryEntity.distinct = true;
        var userList = await users.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", userList));
    }
};