var users = require("../../db/user.js");
var userroles = require("../../db/userrole.js")
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var sd = require("silly-datetime");
var crypto = require("crypto");
var conf = require("../../config/config");
module.exports = {
    /**
     * summary: 添加用户
     * description: 
     * parameters: body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */

    post: async function addUser(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var paramObj = req.body;
            var curUser = req.curUser;
            var password = crypto.createHash('md5').update(paramObj.password.toString()).digest('base64');
            var checkUser = await users.findOne({
                where: { userName: paramObj.userName }
            });
            if (checkUser != null) {
                throw new ApiError("用户名已存在");
            }
            var user = await users.create({
                userName: paramObj.userName,
                mobile: paramObj.mobile,
                password: password,
                status: 0,
                createdBy: curUser.id
            });
            if (paramObj.roleID) {
                await userroles.create({
                    roleID: paramObj.roleID,
                    userID: user.id
                });
            }
            return res.status(200).send(new ApiResult(0, '添加成功'));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message));
            }
            throw error;
        }
    }
};