'use strict';
var grouppush = require("../../db/groupmessage.js");
var members = require("../../db/member.js");
var labels = require("../../db/label.js");
var templates = require("../../db/template.js");
var conf = require("../../config/config");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var tplFormat = require("../../utils/templateFormat");
var wxApi = require("../weixin/common");
var logger = require('../../utils/logger').getLogger();
/**
 * Operations on /grouppush/push
 */
module.exports = {
    get: async function groupPushMessage(req, res, next) {
        try {
            // 1.取所有需要推送的消息
            var curUser = req.curUser;
            var paramObj = light.param2Obj(req.url);
            var idList = paramObj.id.split('|');
            var pushList = await grouppush.findAll({
                where: light.getCondition([
                    { "field": "id", "op": "$in", "value": [idList] },
                    { "field": "status", "op": "$eq", "value": [-1] }
                ])
            });
            if (pushList.length == 0) {
                throw new ApiError("未找到可以推送的消息");
            }
            var tplIDs = pushList.map(x => x.tplID);
            var tpls = await templates.findAll({ where: light.getCondition([{ "field": "id", "op": "$in", "value": [tplIDs] }]) });
            var successCount = 0;
            var faildCount = 0;
            pushList.forEach(async(x) => {
                // 2.取该消息需要推送的分组ID
                var label = await labels.findOne({ where: light.getCondition([{ "field": "id", "op": "$eq", "value": [x.labelID] }]) });
                if (!label) {
                    faildCount++;
                    logger.error('分组推送Error : 未找到分组');
                    return;
                }
                //--生成模板消息的内容
                var template = tpls.find(m => m.id == x.tplID);
                if (!template) {
                    logger.error('分组推送Error : 未找到模板');
                    faildCount++;
                    return;
                }
                var params = JSON.parse(x.params);
                var data = {};
                for (var key in params) {
                    data[key] = {
                        value: params[key].value
                    };
                }
                // 3.取该分组下所有的会员的OpenId
                var con = light.getCondition([
                    { "field": "industryID", "op": "$eq", "value": [label.id] },
                    { "field": "status", "op": "$eq", "value": [0] },
                ])
                var memberList = await members.findAll({
                    where: con
                });
                if (memberList.length == 0) {
                    logger.error('分组推送Error :该分组下没有会员');
                    faildCount++;
                    return;
                }
                var url = conf.wechatUrl + "/#/group/" + x.id;
                var errmsg = "";
                //遍历会员列表进行推送
                memberList.forEach(member => {
                    try {
                        wxApi.sendTemplate(member.openID, template.templateNo, url, data, async function(err, result) {
                            if (err) {
                                errmsg += `消息Id ${x.id} OpenId: ${member.openID} Error : ` + err;
                                logger.error(`批量推送:消息Id ${x.id} OpenId: ${member.openID} Error : ` + err);
                            }
                        });
                    } catch (error) {
                        errmsg += `消息Id ${x.id} OpenId: ${member.openID} Error : ` + err;
                        logger.error(`批量推送:消息Id ${x.id} OpenId: ${member.openID} Error : ` + err);
                    }
                })
                var r = await grouppush.update({
                    status: 0,
                    pushTime: new Date(),
                    updatedBy: curUser.id
                }, {
                    where: { id: x.id }
                });
                r ? successCount++ : faildCount++;
                if (faildCount + successCount == pushList.length) {
                    if (successCount == 0) {
                        throw new ApiError("推送失败。" + errmsg);
                    }
                    res.status(200).send(new ApiResult(0, `推送成功`, errmsg));
                }
            })
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }

};