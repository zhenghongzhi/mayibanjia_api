'use strict';
var groupmessages = require("../../db/groupmessage.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var query = require("../../utils/queryCfg");
var templates = require("../../db/template.js");
var labels = require("../../db/label.js");
var groupfiles = require("../../db/groupfile.js");

/**
 * Operations on /grouppush/list
 */
module.exports = {
    /**
     * summary: 会员分组推送列表
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function listGroupPush(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */

        var pageParams = light.getPageParams(req.url);
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        //得到where条件
        var andParams = [{ "field": "status", "op": "$in", "value": [-1, 0] }];
        var orParams = [];
        if (req.body instanceof Array) {
            andParams = andParams.concat(req.body);
        }
        queryEntity.where = light.getCondition(andParams, orParams);
        queryEntity.include = [{ model: templates },
            { model: labels },
            { model: groupfiles }
        ];
        queryEntity.distinct = true;
        var list = await groupmessages.findAndCountAll(queryEntity);
        return res.status(200).send(new ApiResult(0, "查询成功", list));
    }
};