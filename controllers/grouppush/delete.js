'use strict';
var groupmessages = require("../../db/groupmessage.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var path = require("path");
var fs = require("fs");
/**
 * Operations on /grouppush/delete
 */
module.exports = {
    /**
     * summary: 删除消息,删除消息,删除多个时，使用id1|id2|id3形式把要删除的组合在一起
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    delete: async function deleteGroupMessage(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var curUser = req.curUser;
        var paramObj = light.param2Obj(req.url);
        var idArray = paramObj.id.split('|');
        await groupmessages.update({
            status: -2,
            updatedBy: curUser.id
        }, {
            where: { id: idArray }
        });
        return res.status(200).send(new ApiResult(0, "删除成功"));
    }
};