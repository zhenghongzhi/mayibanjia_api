'use strict';
var groupmessages = require("../../db/groupmessage.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /grouppush/add
 */
module.exports = {
    /**
     * summary: 按分组添加会员
     * description: 
     * parameters: body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function grouppush(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var model = req.body;
            model.status = -1;
            model.createdBy = curUser.id;
            if (!model.params || Object.keys(model.params).length == 0) {
                throw new ApiError("模板参数不能为空");
            }
            model.params = JSON.stringify(model.params);

            var groupmessage = await groupmessages.create(model);
            return res.status(200).send(new ApiResult(0, "添加成功"));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }

    }
};