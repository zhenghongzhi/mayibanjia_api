'use strict';
var groupmessages = require("../../db/groupmessage.js");
var sd = require("silly-datetime");
var groupFile = require("../../db/groupfile");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var light = require("../../utils/util.js");

/**
 * Operations on /grouppush/{id}
 */
module.exports = {
    /**
     * summary: 更新分组消息
     * description: 
     * parameters: id, body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    put: async function updateGroupMessage(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var id = light.getPathParam(req.path, 3);
            if (!id) {
                throw new ApiError("消息ID不可缺少");
            }
            var body = req.body;
            body.updatedBy = curUser.id;
            if (body.params) {
                body.params = JSON.stringify(body.params);
            }
            await groupmessages.update(body, {
                where: { id: id }
            });
            res.status(200).send(new ApiResult(0, "更新成功"));

        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    },
    /**
     * summary: 复制并新建
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    post: async function copyNewGroup(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        try {
            var curUser = req.curUser;
            var id = light.getPathParam(req.path, 3);
            if (!id) {
                throw new ApiError("消息ID不可缺少")
            }
            var oldmsg = await groupmessages.findOne({
                where: { id: id },
                include: [{ model: groupFile }]
            });
            if (oldmsg == null) {
                return res.status(404);
            }
            var newMsg = await groupmessages.create({
                labelID: oldmsg.labelID,
                tplID: oldmsg.tplID,
                remark: oldmsg.remark,
                params: oldmsg.params,
                status: -1,
                createdBy: curUser.id
            });
            oldmsg.groupfiles.forEach(x => {
                groupFile.create({
                    groupMsgId: newMsg.id,
                    fileType: x.fileType,
                    fileName: x.fileName,
                    filePath: x.filePath,
                    createdBy: curUser.id
                });
            })
            return res.status(200).send(new ApiResult(0, "复制成功"));
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }
    }
};