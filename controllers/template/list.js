var templates = require("../../db/template.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var tplFormat = require("../../utils/templateFormat");
module.exports = {
    /**
     * summary: 模版列表
     * description: 
     * parameters: pageSize, pageNo
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function listTemplate(req, res, next) {
        var pageParams = light.getPageParams(req.url);
        //得到where条件
        var andParams = [];
        var orParams = [];
        if (pageParams.searchText) {
            orParams.push({ "field": "title", "op": "$like", "value": [pageParams.searchText] });
            orParams.push({ "field": "templateNo", "op": "$like", "value": [pageParams.searchText] });
            orParams.push({ "field": "OneIndustry", "op": "$like", "value": [pageParams.searchText] });
        }
        //构建查询参数
        var queryEntity = light.getQueryEntity(pageParams);
        queryEntity.where = light.getCondition(andParams, orParams);
        var template = await templates.findAndCountAll(queryEntity);
        if (template.rows && template.rows.length > 0) {
            template.rows.forEach(function(element) {
                if (element.paramList && element.paramList != null && element.paramList != "") {
                    element.paramList = JSON.parse(element.paramList);
                }
            });
        }
        return res.status(200).send(new ApiResult(0, "查询成功", template));
    }
};