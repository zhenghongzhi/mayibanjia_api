'use strict';
var templates = require("../../db/template.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /template/detail
 */
module.exports = {
    /**
     * summary: 模版详情
     * description: 
     * parameters: id
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: async function detailTemplate(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */

        var paramObj = light.param2Obj(req.url);
        var template = await templates.findOne({
            where: { id: paramObj.id }
        });
        return res.status(200).send(new ApiResult(0, '查询成功', template));
    }
};