'use strict';
var templates = require("../../db/template.js");
var light = require("../../utils/util.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
var wxApi = require("../weixin/common");
var tplFormat = require("../../utils/templateFormat");
var logger = require('../../utils/logger').getLogger();

/**
 * Operations on /template/sync
 */
module.exports = {
    /**
     * summary: 同步模版
     * description: 
     * parameters: 
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    get: function syncTpl(req, res, next) {
        try {
            var curUser = req.curUser;
            wxApi.getAllPrivateTemplate((err, result) => {
                if (err) {
                    logger.error('getAllPrivateTemplate Error : ' + err);
                    throw new ApiError(err);
                }
                var list = result.template_list;
                var index = 0; //用来判断是否执行完成 
                list.forEach(async(x) => {
                    var paramList = '[' + (tplFormat.AllFormat(x.content, x.example)).toString() + ']';
                    var model = {
                        templateNo: x.template_id,
                        title: x.title,
                        OneIndustry: x.primary_industry,
                        TwoIndustry: x.deputy_industry,
                        content: x.content,
                        Demo: x.example,
                        status: 0,
                        paramList: paramList,
                        createdBy: curUser.id,
                        updatedBy: curUser.id
                    };
                    if (x.title != "订阅模板消息") {
                        var r = await templates.findOrCreate({ where: { templateNo: x.template_id }, defaults: model });
                        index = r ? index + 1 : index + 1;
                    } else {
                        index += 1;
                    }
                    if (index == list.length) {
                        res.status(200).send(new ApiResult(0, "同步成功"));
                    }

                });

            });
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(200).send(new ApiResult(-1, error.message))
            }
            throw error;
        }

    }
}