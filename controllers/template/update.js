'use strict';
var templates = require("../../db/template.js");
var ApiError = require("../../utils/ApiError.js");
var ApiResult = require("../../utils/ApiResult.js");
/**
 * Operations on /template/update
 */
module.exports = {
    /**
     * summary: 更新参数
     * description: 更新参数信息
     * parameters: body
     * produces: application/xml, application/json
     * responses: 200, 400, 404
     */
    put: async function updateTpl(req, res, next) {
        /**
         * Get the data for response 200
         * For response `default` status 200 is used.
         */
        var body = req.body;
        if (body.paramList) {
            body.paramList = JSON.stringify(body.paramList);
        }
        var template = await templates.update(body, {
            where: { id: body.id }
        });
        return res.status(200).send(new ApiResult(0, "模版更新成功"));
    }
};