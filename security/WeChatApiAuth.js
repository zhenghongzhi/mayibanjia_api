'use strict';
var fans = require("../db/fan.js");
var members = require("../db/member.js");
/**
 * Authorize function for securityDefinitions:WeChatApiAuth
 * type : apiKey
 * description: 
 */
module.exports = async function authorize(req, res, next) {
    //The context('this') for authorize will be bound to the 'securityDefinition'
    //this.name - The name of the header or query parameter to be used for securityDefinitions:WeChatApiAuth apiKey security scheme.
    //this.in - The location of the API key ("query" or "header") for securityDefinitions:WeChatApiAuth apiKey security scheme.

    if (req.path.indexOf("/wechat/") < 0 && req.path.indexOf("/message/") < 0) {
        // next();
    } else {
        var openid = req.headers["x-openid"];
        if (!openid) {
            return res.status(403).send("Please send openid in header");
        }
        var fan = await fans.findOne({ where: { openID: openid } });
        if (fan == null) {
            return res.status(401).send("身份认证失败");
        }
        req.curFan = fan;
        next();

    }
};