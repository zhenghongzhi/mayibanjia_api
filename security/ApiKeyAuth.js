'use strict';
var users = require("../db/user.js");
var userroles = require('../db/userrole.js');
var roles = require("../db/role.js");
/**
 * Authorize function for securityDefinitions:ApiKeyAuth
 * type : apiKey
 * description: 
 */
module.exports = async function authorize(req, res, next) {
    //The context('this') for authorize will be bound to the 'securityDefinition'
    //this.name - The name of the header or query parameter to be used for securityDefinitions:ApiKeyAuth apiKey security scheme.
    //this.in - The location of the API key ("query" or "header") for securityDefinitions:ApiKeyAuth apiKey security scheme.


    //Perform auth here

    var token = req.headers["x-token"];
    if (!token) {
        return res.status(403).send("Please send Token in header");
    }
    var user = await users.findOne({ where: { token: token } });
    if (user == null || user.status == -1 || user.tokenExpireTime < new Date()) {
        return res.status(401).send("身份认证失败");
    }
    req.curUser = user;
    next();
};