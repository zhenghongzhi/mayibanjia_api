var Sequelize = require('sequelize');
var db = require('./db');
var rolemenu = db.define('rolemenu', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    roleId: { type: Sequelize.INTEGER },
    menuId: { type: Sequelize.INTEGER },
    createdAt: { type: Sequelize.DATE },
    updatedAt: { type: Sequelize.DATE },
    updatedBy: { type: Sequelize.STRING },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
module.exports = rolemenu;