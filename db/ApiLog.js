var Sequelize = require('sequelize');
var db = require('./db');
// var label = require("./label");
var ApiLog = db.define('apilog', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    RequestUri: { type: Sequelize.STRING },
    Method: { type: Sequelize.STRING },
    Body: { type: Sequelize.TEXT },
    RequestTime: { type: Sequelize.DATE },
    ClientIP: { type: Sequelize.STRING },
    HttpStatusCode: { type: Sequelize.INTEGER },
    Result: { type: Sequelize.TEXT },
    ExecTime: { type: Sequelize.INTEGER },
    Token: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
module.exports = ApiLog;