var Sequelize = require('sequelize');
var db = require('./db');
var AccessToken = db.define('accessToken', {
    accessToken: { type: Sequelize.STRING, primaryKey: true },
    expireTime: { type: Sequelize.DATE }
}, {
    freezeTableName: true
});
module.exports = AccessToken;