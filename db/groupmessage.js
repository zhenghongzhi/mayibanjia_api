var Sequelize = require('sequelize');
var db = require('./db');
var Template = require("./template");
var Label = require("./label");
var GroupFile = require("./groupfile");
var GroupMessage = db.define('groupmessage', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true, unique: true },
    labelID: { type: Sequelize.INTEGER },
    tplID: { type: Sequelize.INTEGER },
    pushTime: { type: Sequelize.DATE },
    params: { type: Sequelize.STRING(500) },
    status: { type: Sequelize.INTEGER },
    createdAt: { type: Sequelize.DATE },
    updatedAt: { type: Sequelize.DATE },
    updatedBy: { type: Sequelize.STRING },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
GroupMessage.belongsTo(Template, { foreignKey: 'tplID', sourceKey: 'id' })
GroupMessage.belongsTo(Label, { foreignKey: 'labelID', sourceKey: 'id' })
GroupMessage.hasMany(GroupFile, { foreignKey: 'groupMsgId', sourceKey: 'id' })

module.exports = GroupMessage;