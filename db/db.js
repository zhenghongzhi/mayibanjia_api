    var Sequelize = require('sequelize');
    var log = require("../utils/logger");
    var config = require('../config/config');
    module.exports = new Sequelize(config.DataBaseName, config.MySqlUserName, config.MySqlPass, {
        host: config.MySqlHost, // 数据库地址
        port: config.MySqlPort,
        dialect: 'mysql', // 指定连接的数据库类型
        timezone: "+08:00",
        pool: {
            max: 5, // 连接池中最大连接数量
            min: 0, // 连接池中最小连接数量
            idle: 10000 // 如果一个线程 10 秒钟内没有被使用过的话，那么就释放线程
        },
        logging: function(sql) { //设置logging:false 时将关闭日志
            log.getLogger("applog").info(sql);
        }
    });