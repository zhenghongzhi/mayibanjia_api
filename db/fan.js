var Sequelize = require('sequelize');
var db = require('./db');
// var label = require("./label");
var fan = db.define('fan', {
    openID: { type: Sequelize.STRING, primaryKey: true },
    subscribe: { type: Sequelize.INTEGER },
    nickname: { type: Sequelize.STRING },
    sex: { type: Sequelize.STRING },
    city: { type: Sequelize.STRING },
    country: { type: Sequelize.STRING },
    province: { type: Sequelize.STRING },
    headImgUrl: { type: Sequelize.STRING },
    subscribeTime: { type: Sequelize.DATE },
    language: { type: Sequelize.STRING },
    remark: { type: Sequelize.STRING },
    groupID: { type: Sequelize.STRING },
    UnionId: { type: Sequelize.STRING },
    createdAt: { type: Sequelize.DATE },
    updatedAt: { type: Sequelize.DATE },
    updatedBy: { type: Sequelize.STRING },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
module.exports = fan;