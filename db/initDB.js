const AccessToken = require("./accessToken");
const Fan = require("./fan");
const GroupFile = require("./groupfile");
const GroupMessage = require("./groupmessage");
const Label = require("./label");
const Member = require("./member");
const Role = require("./role");
const RoleMenu = require("./role");
const SingleFile = require("./singlefile");
const SingleMessage = require("./singlemessage");
const Template = require("./template");
const User = require("./user");
const UserRole = require("./userrole");
const ApiLog = require("./ApiLog");

async function Init() {
    await AccessToken.sync();
    await Fan.sync();
    await Label.sync();
    await Template.sync();

    await Member.sync();

    await SingleMessage.sync();
    await SingleFile.sync();

    await GroupMessage.sync();
    await GroupFile.sync();

    await Role.sync();
    await RoleMenu.sync();
    await User.sync();
    await UserRole.sync();
    await ApiLog.sync();
    await InitData();
}

async function InitData() {
    var fan = await Fan.findOne({ where: { openID: "openid" } });
    if (fan == null) {
        await Fan.create({
            city: '',
            industryID: '',
            country: '',
            groupID: 0,
            headImgUrl: '',
            language: '',
            nickname: '未绑定',
            openID: 'openid',
            province: '',
            remark: '',
            sex: '1',
            subscribeTime: '2017-01-01',
            UnionId: ''
        });
    }

    var defaultRole = await Role.findOne({ where: { name: "admin" } });
    if (defaultRole == null) {
        defaultRole = await Role.create({
            name: 'admin',
            roleInfo: '超级管理员',
            status: 0,
            menus: '[]'
        });
    }
    var adminUser = await User.findOne({ where: { userName: "admin" } });
    if (adminUser == null) {
        adminUser = await User.create({
            userName: 'admin',
            mobile: '',
            password: '4QrcOUm6Wau+VuBX8g+IPg==',
            status: 0,
            roleID: defaultRole.id,
            createdBy: 'admin',
        });
    }

    var adminRole = await UserRole.findOne({ where: { userID: adminUser.id } });
    if (adminRole == null) {
        adminRole = await UserRole.create({
            userID: adminUser.id,
            roleID: defaultRole.id,
            createdBy: 'admin'
        });
    }
}

module.exports = {
    Init
};