var Sequelize = require('sequelize');
var db = require('./db');
var Role = require("./role");
var UserRole = db.define('userrole', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    userID: { type: Sequelize.INTEGER },
    roleID: { type: Sequelize.INTEGER },
    createdAt: { type: Sequelize.DATE },
    updatedAt: { type: Sequelize.DATE },
    updatedBy: { type: Sequelize.STRING },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
UserRole.belongsTo(Role, { foreignKey: 'roleID', sourceKey: 'id' })
module.exports = UserRole;