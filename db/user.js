var Sequelize = require('sequelize');
var UserRole = require("./userrole");
var roles = require("./role");

var db = require('./db');
var User = db.define('user', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    userName: { type: Sequelize.STRING },
    mobile: { type: Sequelize.STRING },
    roleID: { type: Sequelize.INTEGER },
    password: { type: Sequelize.STRING },
    status: { type: Sequelize.INTEGER },
    token: { type: Sequelize.STRING },
    tokenExpireTime: { type: Sequelize.DATE },
    createdAt: { type: Sequelize.DATE },
    updatedAt: { type: Sequelize.DATE },
    updatedBy: { type: Sequelize.STRING },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
User.hasMany(UserRole, { foreignKey: 'userID', sourceKey: 'id' })
module.exports = User;