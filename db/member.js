var Sequelize = require('sequelize');
var db = require('./db');
var Label = require("./label");
var Fan = require("./fan");
var Member = db.define('member', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    memberNo: { type: Sequelize.STRING },
    companyName: { type: Sequelize.STRING },
    industryID: { type: Sequelize.INTEGER },
    contacts: { type: Sequelize.STRING },
    mobile: { type: Sequelize.STRING },
    address: { type: Sequelize.STRING },
    openBank: { type: Sequelize.STRING },
    openBranch: { type: Sequelize.STRING, defaultValue: '' },
    openName: { type: Sequelize.STRING },
    bankNo: { type: Sequelize.STRING },
    openID: { type: Sequelize.STRING, defaultValue: "openid" },
    status: { type: Sequelize.INTEGER },
    companyDesc: { type: Sequelize.STRING },
    isBusi: { type: Sequelize.BOOLEAN, defaultValue: true }, //是否有商业关系（是，必输银行信息）
    createdAt: { type: Sequelize.DATE },
    updatedAt: { type: Sequelize.DATE },
    updatedBy: { type: Sequelize.STRING },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
Member.belongsTo(Fan, { foreignKey: 'openID', sourceKey: 'openID' });
Member.belongsTo(Label, { foreignKey: 'industryID' });
module.exports = Member;