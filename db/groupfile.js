var Sequelize = require('sequelize');
var db = require('./db');
var groupfile = db.define('groupfile', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    groupMsgId: { type: Sequelize.INTEGER },
    fileType: { type: Sequelize.INTEGER },
    fileName: { type: Sequelize.STRING },
    filePath: { type: Sequelize.STRING },
    createdAt: { type: Sequelize.DATE },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
module.exports = groupfile;