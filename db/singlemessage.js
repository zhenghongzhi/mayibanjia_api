var Sequelize = require('sequelize');
var db = require('./db');
var Template = require("./template");
var Member = require("./member");
var SingleFile = require("./singlefile");
var SingleMessage = db.define('singlemessage', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true, unique: true },
    title: { type: Sequelize.STRING },
    memberId: { type: Sequelize.INTEGER },
    CCMemberId: { type: Sequelize.INTEGER },
    tplID: { type: Sequelize.INTEGER },
    openID: { type: Sequelize.STRING },
    pushTime: { type: Sequelize.DATE },
    params: { type: Sequelize.STRING(2000) },
    remark: { type: Sequelize.STRING },
    status: { type: Sequelize.INTEGER },
    createdAt: { type: Sequelize.DATE },
    updatedAt: { type: Sequelize.DATE },
    updatedBy: { type: Sequelize.STRING },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
SingleMessage.belongsTo(Template, { foreignKey: 'tplID', sourceKey: 'id' })
SingleMessage.belongsTo(Member, { foreignKey: 'memberId', sourceKey: 'id' })
SingleMessage.hasMany(SingleFile, { foreignKey: 'singleMsgId', sourceKey: 'id' })

module.exports = SingleMessage;