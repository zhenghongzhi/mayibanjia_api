var Sequelize = require('sequelize');
var db = require('./db');

var template = db.define('template', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true, unique: true },
    templateNo: { type: Sequelize.STRING },
    title: { type: Sequelize.STRING },
    OneIndustry: { type: Sequelize.STRING },
    TwoIndustry: { type: Sequelize.STRING },
    content: { type: Sequelize.STRING },
    Demo: { type: Sequelize.STRING },
    paramList: { type: Sequelize.STRING(500) },
    status: { type: Sequelize.INTEGER },
    createdAt: { type: Sequelize.DATE },
    updatedAt: { type: Sequelize.DATE },
    updatedBy: { type: Sequelize.STRING },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
module.exports = template;