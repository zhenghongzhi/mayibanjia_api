var Sequelize = require('sequelize');
var db = require('./db');
var role = db.define('role', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    name: { type: Sequelize.STRING },
    roleInfo: { type: Sequelize.STRING },
    status: { type: Sequelize.INTEGER },
    menus: { type: Sequelize.STRING },
    createdAt: { type: Sequelize.DATE },
    updatedAt: { type: Sequelize.DATE },
    updatedBy: { type: Sequelize.STRING },
    createdBy: { type: Sequelize.STRING }
}, {
    freezeTableName: true
});
module.exports = role;